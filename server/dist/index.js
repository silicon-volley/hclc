'use strict';

var _Jugador = require('./src/model/Jugador');

var _Jugador2 = _interopRequireDefault(_Jugador);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.get('/', function (req, res) {
    res.send('<h1>Hello world</h1>');
});

io.on('connection', function (socket) {
    var juga = new _Jugador2.default(socket);
});

http.listen(3000, function () {
    console.log('listening on *:3000');
});