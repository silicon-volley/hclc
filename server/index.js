'use strict';
import Jugador from './src/model/Jugador'
import GestorSala from './src/controler/GestorSala'
import event from './src/protocolo/eventos'
import utils from "./src/utils/utils";
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

let usuarios = {}
let gestorSala = new GestorSala(io)

http.listen(3000, function(){
    console.log('listening on *:3000');
});


io.on(event.CONECTANDO, function(socket) {
    // Nuevo usuario conectado
    let jugador = new Jugador(socket);
    usuarios[socket.id] = socket
    jugador.escuchaEventosSet()
    console.log('El numero de usuarios conectados es '+Object.keys(usuarios).length)
    jugadorConectado(socket, jugador);
   // socket.emit(event.MENSAJE_AVISADOR,{text:"ESTO ES UNA PRUEBA BONITA"})
    // setTimeout(() => {
    //     socket.emit(event.FIN_TEMPORIZADOR,{st:"Se ha acabado el tiempo"})
    // },12000)
    // Usuario reconcectando
    socket.on(event.RECONECTANDO, function () {
        if(jugador.sala !== null) {
            console.log('Se ha reconectado el jugador ' + jugador.id)
            if (jugador.isCreador) {
                gestorSala.eliminarSala(jugador.sala.id)
                jugador.sala = null
            }else{
                gestorSala.salas[jugador.sala.id].eliminarJugador(jugador)
                jugador.sala = null
            }
        }else{
            console.log('Se ha unido el usuario:' + socket.id)
        }
    })

    // Usuario desconectado
    socket.on(event.DESCONECTANDO, function () {
        console.log('Se ha desconectado el jugador ' + jugador.id)
        if(jugador.sala !== null) {
            if (jugador.isCreador && jugador.sala.isAbierta) {
                gestorSala.eliminarSala(jugador.sala.id)
            }else{
                gestorSala.salas[jugador.sala.id].eliminarJugador(jugador)
            }
        }
        delete usuarios[socket.id]
        console.log('El numero de usuarios conectados es ' + Object.keys(usuarios).length)
    })

    // Enviar a inicio
    event.enviarRuta(socket,'/')
})
// Escuchas a los eventos de un jugador conectado
function jugadorConectado(socket, jugador) {
    // Evento unir sala
    let funUnirSala = (response) => {
        console.log('El jugador:' + jugador.id + ' se quiere unir a la sala ' + response.id)
        var sala = gestorSala.unirSala(response.id, jugador)
        if (sala !== null) {
            jugador.enviarDetroSala(sala)
            // event.eliminarEvento(eventos,socket)
        }
    }
    socket.on(event.UNIR_A_LA_SALA, funUnirSala)
    // Evento crear sala
    let funCrearSala = () => {
        console.log('El jugador ' + jugador.id + ' quiere crear una sala')
        var sala = gestorSala.crearSala(jugador)
        if (sala !== null) {
            console.log('Se ha creado la sala ' + sala.id)
            jugador.enviarDetroSala(sala)
            //  event.eliminarEvento([eventos[0]],socket)
        }
    }
    socket.on(event.CREAR_SALA, funCrearSala)
    let eventos  = [{name:event.CREAR_SALA,fun:funCrearSala}, {name:event.UNIR_A_LA_SALA,fun:funUnirSala}]

}



