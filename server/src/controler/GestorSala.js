import Sala from '../model/Sala'
import utils from '../utils/utils'
import event from "../protocolo/eventos";

export default class GestorSala {
    constructor(_io) {
        this.io = _io
        this.salas = {}
        console.log('gestor creado')
    }

    crearSala(jugCreador) {
        var sala = jugCreador.sala
        if (sala === null) {
            sala = new Sala(utils.random(), jugCreador.id, this.io,this)
            this.salas[sala.id] = sala
            var params = {
                id: sala.id
            }
            jugCreador.socket.emit(event.ID_SALA_CREADA, params)
            console.log('Envindo el id de la partida al creador.ID: ' + sala.id)
        } else {
            console.log('El jugador ya esta unido a la partida ' + jugCreador.sala.id)
            event.enviarError(jugCreador.socket, 'Ya esta unido a una sala.')
            jugCreador.socket.emit(event.NO_DENTRO_SALA)
            sala = null
        }
        return sala
    }

    eliminarSala(codigo) {
        var sala = this.salas[codigo]
        sala.borrarSala()
        delete this.salas[codigo]
        console.log('La sala ' + sala.id + ' ha sido eliminada')
    }


    // Unir a un jugador a una sala, se comprueba si ya esta en alguna sala o la sala no existe
    unirSala(codigoSala, jugador) {
        var sala = jugador.sala
        if (sala === null) {
            sala = this.salas[codigoSala]
            if (sala) {
                sala.unirJugador(jugador)
            } else {
                console.log('La sala con el id ' + codigoSala + ' no existe')
                event.enviarError(jugador.socket, 'La sala con el id ' + codigoSala + ' no existe')
                event.enviarRuta(jugador.socket, '/')
                sala = null
            }
        } else {
            console.log('El jugador ya esta unido a la partida ' + sala.id)
            event.enviarError(jugador.socket, 'Ya esta unido a una sala.')
            sala = null
        }
        return sala
    }
}
