import eventos from '../protocolo/eventos'

export default class Jugador {
    constructor(_socket) {
        this.socket = _socket
        this.id = _socket.id
        this.nombre = 'Id: ' + this.id
        this.isCreador = false
        this.sala = null
        this.puntos=0
        // this.event = {}
    }

    escuchaEventosSet() {
        console.log('Escuchando eventos set')
        let that = this
        let funSetNombre = (data) => {
            console.log(data)
            console.log('El jugador con id ' + that.id + ' guarda este nombre ' + data.nombre)
            if(data.nombre === ''){
                data.nombre = 'Anonimo'
            }
            that.nombre = data.nombre
            if (that.sala !== null) {
                that.sala.actualizarJugadores()
            }
        }
        this.socket.on(eventos.SET_NOMBRE, funSetNombre)

        let funIsCreador = (data) => {
            console.log('El jugador con id ' + that.id + ' pregunta si es creador, (' + this.isCreador + ')')
            var params = {
                isCreador: this.isCreador
            }
            this.socket.emit(eventos.IS_CREADOR, params)
        }
        this.socket.on(eventos.IS_CREADOR, funIsCreador)

        // this.event.conectado = [{name: eventos.SET_NOMBRE, fun: funSetNombre}, {
        //     name: eventos.IS_CREADOR,
        //     fun: funIsCreador
        // }]
    }

    eliminarEventos(even) {
        eventos.eliminarEvento(even, this.socket)
    }

    // Escuchas en la sala de espera
    salaDeEspera() {
        let that = this
        // Solo si es creador
        if (this.isCreador) {
            this.socket.on(eventos.EMPEZAR_PARTIDA, function (data) {
                if (data.idSala ? data.idSala : null === that.sala.id) {
                    console.log('La sala ' + data.idSala + ' ha sido empezada por ' + that.id)
                    that.sala.empezarPartida()
                } else {
                    console.log('Las sala ' + data.idSala + ' no coincide ' + that.id + 'no se puede empezar la partida')
                    eventos.enviarError(that.socket, 'No puedes empezar una sala a la que no estas unido')
                }
            })
        }
    }

    enviarDetroSala(sala) {
        // Parametos de la llamada dentro-sala
        var params = {
            id: sala.id,
            numeroJugadores: Object.keys(sala.jugadores).length
        }
        this.socket.emit(eventos.DENTRO_SALA, params)
    }


    setSala(sala) {
        this.sala = sala
    }
}
