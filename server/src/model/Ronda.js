import eventos from "../protocolo/eventos";

export default class Ronda {
    constructor(_sala) {
        this.cartaNegra = null
        this.cartasVotadas = []
        this.timerSeleccionar = null
        this.timerVotacion = null
        this.sala = _sala
        this.tiempoV = 0
        this.tiempoS = 0
        this.frase = ""

        this.cartas = []
    }

    setVotada(carta) {
        var cartaAux;
        if (this.cartas.filter(aux => aux.id === carta.id).length >= 1) {
            cartaAux = this.cartas.filter(aux => aux.id === carta.id)[0]
            cartaAux.puntos++
        } else {
            this.cartas.push({
                jugador: carta.idJugador,
                puntos: 1,
                mensaje: carta.mensaje,
                id: carta.id
            })
        }
    }

    getGanador() {
        console.log('En ganador antes de ordenar ' + this.cartas)
        var ganador = null
        this.cartas.sort((a, b) => {
            return  b.puntos - a.puntos;
        });
        console.log('En ganador despues de ordenar ' + this.cartas)
        if (this.cartas.length === 0) {
            return null
        }
        let arrayEmpate = this.cartas.filter(aux => aux.puntos === this.cartas[0].puntos)
        if (arrayEmpate.length === 1) {
            ganador = arrayEmpate[0]
        } else {
            ganador = arrayEmpate[Math.floor(Math.random() * arrayEmpate.length)]
        }
        console.log('El ganador es: ' + ganador.jugador + ' con la carta ' + ganador.mensaje + ' con ' + ganador.puntos + ' puntos')
        return ganador
    }

    finRonda() {
        if (this.timerSeleccionar) {
            this.timerSeleccionar.stop()
        }
        if (this.timerVotacion) {
            this.timerVotacion.stop()
        }
    }

    iniciarTemporizadorElegir() {
        console.log('Iniciando temporizador eleccion carta de la sala ' + this.sala.id)
        this.tiempoS = 30;
        this.timerSeleccionar = setInterval(() => {
            if (this.tiempoS <= 0) {
                clearInterval(this.timerSeleccionar)
                console.log('Parando el temporizador de eleccion de la sala ' + this.sala.id)
                this.sala.enviarASala(eventos.MENSAJE_AVISADOR, {text: "Se ha acabado el tiempo"})
                this.sala.enviarASala(eventos.GET_CARTA_SELECCIONADA)
            }
            this.frase = "Elige una carta. Quedan " + this.tiempoS + " segundos"
            this.sala.enviarASala(eventos.MENSAJE_AVISADOR, {text: this.frase})
            this.tiempoS--;

        }, 1000)
    }

    iniciarTemporizadorVotar() {
        console.log('Iniciando temporizador eleccion carta de la sala ' + this.sala.id)
        this.tiempoV = 30;
        this.timerVotacion = setInterval(() => {
            if (this.tiempoV <= 0) {
                clearInterval(this.timerVotacion)
                console.log('Parando el temporizador de votacion de la sala ' + this.sala.id)
                this.sala.enviarASala(eventos.MENSAJE_AVISADOR, {text: "Se ha acabado el tiempo"})
                this.sala.enviarASala(eventos.GET_CARTA_VOTADA)
            }
            this.frase = "Vota una carta. Quedan " + this.tiempoV + " segundos"
            this.sala.enviarASala(eventos.MENSAJE_AVISADOR, {text: this.frase})
            this.tiempoV--;

        }, 1000)
    }

    finTemporizadorElegir() {
        clearInterval(this.timerSeleccionar)
    }

    finTemporizadorVotar() {
        clearInterval(this.timerVotacion)
    }
}
