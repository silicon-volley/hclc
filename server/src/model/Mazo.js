import {cartasBlancas, cartasNegras} from "../bbdd/cartas"

export default class Mazo {
    constructor() {
        this.cartas = JSON.parse(JSON.stringify(cartasBlancas))
        this.cartasNegras = JSON.parse(JSON.stringify(cartasNegras))
        this.barajar();

    }

    barajar() {
        this.barajarBase(this.cartas)
        this.barajarBase(this.cartasNegras)
    }

    barajarBase(cartas) {
        let x, y, aux
        for (let i = 0; i < cartas.length; i++) {
            x = Math.ceil(Math.random() * cartas.length - 1)
            y = Math.ceil(Math.random() * cartas.length - 1)
            aux = cartas[x]
            cartas[x] = cartas[y]
            cartas[y] = aux
        }
    }

    getCartas(num) {
        if (num > 0) {
            if (num > this.cartas.length) {
                this.cartas = JSON.parse(JSON.stringify(cartasBlancas))
            }
            let r = this.cartas.splice(0, num)
            r.forEach(aux => {
                aux.seleccionada = false
            })
            return r;
        }
        return []
    }

    getCartaNegra() {
        if (1 > this.cartasNegras.length) {
            this.cartasNegras = JSON.parse(JSON.stringify(cartasNegras))
        }
        let r = this.cartasNegras.splice(0, 1)[0]
        return r;
    }
}
