import eventos from "../protocolo/eventos"
import Mazo from './Mazo'
import {NUM_CARTAS, BARRA_BAJA} from "../bbdd/cartas"
import Ronda from "./Ronda";


const ESTADO_INICIO_PARTIDA = 'inicioPartida'
const ESTADO_INICIO_SELECCION = 'inicioSeleccion'
const ESTADO_INICIO_VOTACION = 'inicioVotacion'
const ESTADO_FIN_SELECCION = 'finSeleccion'
const ESTADO_FIN_VOTACION = 'finVotacion'
const ESTADO_FIN_RONDA = 'finRonda'
const RONDAS = 5

export default class Partida {
    constructor(_sala) {
        this.sala = _sala
        this.mazo = new Mazo()
        this.cartasSelecionadas = []
        this.cartasVotadas = []
        this.rondas = []
        this.iterRonda = 0
        this.frase = ""
        // iniciando rondas
        for (var i = 0; i < 5; i++) {
            this.rondas[i] = new Ronda(_sala)
        }

        // iniciando jugadores
        let jug
        let aux
        for (var id in _sala.jugadores) {
            jug = _sala.jugadores[id]
            this.eventosPartida(jug)
        }
    }

    eventosPartida(jug) {
        let socket = jug.socket
        console.log('El jugador con id ' + socket.id + ' escucha eventos partida')
        let that = this

        // Evento get cartas
        let funGetCartas = (data) => {
            console.log('El jugador con id ' + socket.id + ' tiene ' + data.numCartas + ' cartas')
            socket.emit(eventos.SET_CARTAS, this.mazo.getCartas(NUM_CARTAS - data.numCartas))
        }
        socket.on(eventos.GET_CARTAS, funGetCartas)

        // Evento sincronizar estados

        jug[ESTADO_INICIO_PARTIDA] = false
        jug[ESTADO_INICIO_SELECCION] = false
        jug[ESTADO_INICIO_VOTACION] = false
        jug[ESTADO_FIN_SELECCION] = false
        jug[ESTADO_FIN_VOTACION] = false
        jug[ESTADO_FIN_RONDA] = false
        let funSinEstado = (data) => {
            console.log(data.estado + ' sincronizado el jugador ' + jug.id + ' de la sala ' + that.sala.id)
            jug[data.estado] = true
            var cont = 0;
            for (var id in that.sala.jugadores) {
                if (that.sala.jugadores[id][data.estado]) {
                    cont++
                }
            }
            if (cont === Object.keys(that.sala.jugadores).length) {
                for (var id in that.sala.jugadores) {
                    that.sala.jugadores[id][data.estado] = false
                }
                console.log(data.estado + ' todos sincronizados de la sala ' + that.sala.id)
                that.eventosSincronizacion(data.estado, that.rondaActual)
            }
        }
        socket.on(eventos.SINCRONIZAR_ESTADO, funSinEstado)

        // Evento get carta seleccionada
        let funGetCartaSelect = (data) => {
            if (data) {
                console.log('El jugador ' + jug.id + ' ha seleccionado la carta con id ' + data.id)
                data.idJugador = jug.id
                this.cartasSelecionadas.push(data)
            }
        }
        socket.on(eventos.GET_CARTA_SELECCIONADA, funGetCartaSelect)


        // Evento get carta votada
        let funGetCartaVotada = (data) => {
            if (data) {
                console.log('El jugador ' + jug.id + ' ha votado la carta con id ' + data.id + ' del jugador ' + data.idJugador)
                //  TODO revisar data.seleccionada = false
                this.rondaActual.setVotada(data)
            }
        }
        socket.on(eventos.GET_CARTA_VOTADA, funGetCartaVotada)
    }

    eventosSincronizacion(estado, ronda) {
        switch (estado) {
            case(ESTADO_INICIO_PARTIDA):
                console.log('Todos los jugadores estan sincroniados en inicio')
                this.iniciarRonda()
                break

            case(ESTADO_INICIO_SELECCION):
                this.rondaActual.iniciarTemporizadorElegir()
                this.cartasSelecionadas = []
                break

            case(ESTADO_INICIO_VOTACION):
                this.rondaActual.iniciarTemporizadorVotar()
                break

            case(ESTADO_FIN_SELECCION):
                //Avisar inicio votacion
                this.sala.enviarASala(eventos.SET_CARTAS_VOTACION, this.cartasSelecionadas)
                break

            case(ESTADO_FIN_VOTACION):
                this.cartasVotadas = []
                var mejorCarta = this.rondaActual.getGanador()
                var jugadorGanador = this.sala.jugadores[mejorCarta.jugador]
                console.log('El gandor de la ronda ' + this.iterRonda + ' de la sala ' + this.sala.id + ' es ' + jugadorGanador.nombre)
                jugadorGanador.puntos = jugadorGanador.puntos + 10
                console.log(jugadorGanador.puntos + ' puntos')
                var textoCarta = this.rondaActual.cartaNegra.mensaje
                var textoGanadora = ' [ ' + mejorCarta.mensaje + ' ] '
                textoCarta = textoCarta.replace(BARRA_BAJA, textoGanadora) //Sustituye la barra baja por el mensjae de la carta ganadora
                var text = textoCarta + ' (' + jugadorGanador.nombre + ')'
                this.sala.enviarASala(eventos.MENSAJE_AVISADOR, {text: text})
                this.iterRonda++
                if (this.iterRonda < RONDAS) {
                    setTimeout(() => {
                        this.iniciarRonda()
                    }, 3000)
                } else {
                    console.log('Fin partida')
                    this.sala.cambiarRuta('/sala/' + this.sala.id + '/puntuaciones')
                    setTimeout(() => {
                            var arrayJugador = []
                            for (var player in this.sala.jugadores) {
                                arrayJugador.push(this.sala.jugadores[player].nombre, this.sala.jugadores[player].puntos)
                            }
                            this.sala.enviarASala(eventos.FIN_PARTIDA, arrayJugador)
                        },
                        500)
                }
                break
            case(ESTADO_FIN_RONDA):

                break
        }
    }

    iniciarRonda() {
        console.log('Inicio de la ronda ' + this.iterRonda + ' de la sala ' + this.sala.id)
        this.sala.enviarASala(eventos.MENSAJE_AVISADOR, {text: "Ronda " + (this.iterRonda + 1)})
        this.rondaActual = this.rondas[this.iterRonda]
        this.rondaActual.cartaNegra = this.mazo.getCartaNegra()
        this.sala.enviarASala(eventos.SET_CARTA_NEGRA, this.rondaActual.cartaNegra)
        this.sala.enviarASala(eventos.CUANTAS_CARTAS_TIENES)
    }

    eliminarPartida() {
        this.rondaActual.finTemporizadorElegir()
        this.rondaActual.finTemporizadorVotar()
    }
}
