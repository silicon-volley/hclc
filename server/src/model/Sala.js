import eventos from "../protocolo/eventos";
import Partida from "./Partida";

export default class Sala {

    constructor(_id, _idCreador, _io, _gestorSala) {
        this.id = _id
        this.idCreador = _idCreador
        this.io = _io
        this.jugadores = []
        this.partida = null
        this.isAbierta = true
        this.gestorSala = _gestorSala
    }

    unirJugador(jugador) {
        this.jugadores[jugador.id] = jugador
        //Comprobamos si es el creador de la sala
        if(this.isAbierta){
            if (jugador.id === this.idCreador) {
                jugador.isCreador = true
            }
            jugador.socket.join(this.id, () => {
                console.log('El jugador ' + jugador.id + ' se ha unido a la sala ' + this.id);
                jugador.setSala(this)
                this.actualizarJugadores();
            })
            jugador.salaDeEspera()
        }else{
            eventos.enviarError(jugador.socket,'La partida ya ha empezado')
            eventos.enviarRuta(jugador.socket, '/')
        }
    }

    empezarPartida() {
        this.isAbierta = false
        this.partida = new Partida(this)
        this.io.to(this.id).emit(eventos.EMPIEZA_PARTIDA)
        eventos.enviarRuta(this.io.to(this.id),'/sala/' + this.id + '/partida')
    }

    actualizarJugadores() {
        console.log('Actualizando los datos de los jugadores de la sala '+this.id)
        var params = []
        let jug
        for (var aux in this.jugadores) {
            jug = this.jugadores[aux]
            params.push({
                id: jug.id,
                nombre: jug.nombre
            })
        }
        this.enviarASala(eventos.SET_JUGADORES, params)
    }
    borrarSala() {
        this.enviarASala(eventos.BORRAR_SALA)
        if(!this.isAbierta){
            this.partida.eliminarPartida()
        }
        for (var aux in this.jugadores) {
            var jug = this.jugadores[aux]
            jug.sala = null
            jug.isCreador = false
            eventos.enviarError(jug.socket,'La sala ha sido eliminada')
            eventos.enviarRuta(jug.socket,'/')
        }
        delete this.jugadores
        console.log('Avisando ha los jugadores de que la sala '+this.id+' se ha borrado')
    }
    eliminarJugador(jugador) {
        delete this.jugadores[jugador.id]
        if(!this.isAbierta){
            this.actualizarJugadores();
        }
        if(Object.keys(this.jugadores).length === 0){
            this.eliminarSala()
        }
        console.log('El jugador con id '+jugador.id+' se ha ido de la sala')
    }

    enviarASala(evento,params){
        this.io.to(this.id).emit(evento, params)
    }
    cambiarRuta(ruta){
        eventos.enviarRuta(this.io.to(this.id),ruta)
    }
    eliminarSala(){
        this.gestorSala.eliminarSala(this.id)
    }

}
