export default {
    UNIR_A_LA_SALA: 'unirSala',
    CREAR_SALA: 'crearSala',
    MESAJE_DE_ERROR: 'msgError',
    SET_JUGADORES: 'setJugadores',
    BORRAR_SALA: 'borrarSala',
    DENTRO_SALA: 'dentroSala',
    NO_DENTRO_SALA: 'noDentroSala',
    SET_NOMBRE: 'setNombre',
    EMPEZAR_PARTIDA: 'empezarPartida',
    RECONECTANDO: 'reconnection',
    CONECTANDO: 'connection',
    DESCONECTANDO: 'disconnect',
    ID_SALA_CREADA: 'idSalaCreada',
    IS_CREADOR: 'isCreador',
    ENVIAR_RUTA: 'enviarRuta',
    CUANTAS_CARTAS_TIENES: 'cuantasCartasTienes',
    SET_CARTAS: 'setCartas',
    LISTO_PARA_RONDA: 'listoParaRonda',
    INICIO_TEMPORIZADOR: 'inicioTemporizador',
    FIN_TEMPORIZADOR: 'finTemporizador',
    MENSAJE_AVISADOR: 'mensajeAvisador',
    GET_CARTAS: 'getCartas',
    GET_CARTA_SELECCIONADA: 'getCartaSeleccionada',
    GET_CARTA_VOTADA: 'getCartaVotada',
    SINCRONIZAR_ESTADO: 'sincronizarEstado',
    SET_CARTAS_VOTACION: 'setCartasVotacion',
    FIN_PARTIDA: 'finPartida',
    SET_CARTA_NEGRA: 'setCartaNegra',
    REINICIAR: 'reiniciar',
    enviarError(socket, msgErro){
        socket.emit(this.MESAJE_DE_ERROR,msgErro)
    },
    eliminarEvento(eventos,socket){
        for(var i = 0; i < eventos.length ; i++ ){
            var aux = eventos[i]
            socket.removeListener(aux.name,aux.fun)
        }
    },
    enviarRuta(socket, ruta){
       var params = {
           path:ruta
       }
       socket.emit(this.ENVIAR_RUTA,params)
    }
}
