﻿# HCLC

> Proyecto de Metodología de la programación, URJC

## Como iniciar el proyecto desde windows
1- Instalar node https://nodejs.org/es/

2- Ejecutar los ficheros install-dependencies-client.bat y install-dependencies-server.bat para instalar las dependecias del proyecto

3- Para arrancar el servidor primero ejecutas el fichero star-server.bat

4- Seguidamente ejecutas el fichero star-client.bat

5- Para finalizar desde cualquier navegador accede a http://localhost:8080


##Iniciar el proyecto en otros sistemas operativos

1- Instalar node https://nodejs.org/es/

#Instalar dependencias

1- Abrir el terminal

2- Ir al directorio front-bootstrap

3- Hacer npm install

4- Ir al directorio server

5- Hacer npm install

#Iniciar el proyecto

1- Para arrancar el cliente ir al directorio front-bootstrap y hacer npm run start

2- Para arrancar el servidor ir al directorio server y hacer npm run start-server

3- Acceder mediante el navegador a http://localhost:8080




