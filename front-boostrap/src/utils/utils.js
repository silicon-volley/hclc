import Vue from 'vue'
export default {
  mensajeError(that, msg) {
    that.$toasted.show(msg, {
      theme: "bubble",
      position: "bottom-left",
      duration: 5000
    })
  },
  loadingShow(){
    loader = Vue.$loading.show()
  },
  mensajeCopiarSala(that, msg){
    that.$toasted.show(msg, {
      theme: "toasted-primary",
      position: "top-right",
      duration: 5000
    })
  },
}
