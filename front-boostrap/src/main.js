// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueSocketio from 'vue-socket.io-extended'
import io from 'socket.io-client'
import BootstrapVue from 'bootstrap-vue'
import Toasted from 'vue-toasted'
import Loading from 'vue-loading-overlay'

Vue.use(Loading)
Vue.use(Toasted)
Vue.use(BootstrapVue)
Vue.config.productionTip = false
//Add conexion socket server

Vue.use(VueSocketio, io('http://localhost:3000'));

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
