import Vue from 'vue'
import Router from 'vue-router'
import Sala from '../ventanas/ventanaSala/sala'
import Espera from '../ventanas/ventanaSala/espera'
import Partida from '../ventanas/ventanaSala/partida'
import Inicio from '../ventanas/inicio'
import puntuaciones from "../ventanas/ventanaSala/puntuaciones";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/sala/:idSala',
      name: 'Sala',
      component: Sala,
      props: (query) => {
        return query.params},
      children: [
        {
          path: 'espera',
          component: Espera,
          props: (query) => {
            return query.params}
        }
          ,
        {
          path: 'partida',
          component: Partida,
          props: (query) => {
            return query.params},
        },
        {
          path: 'puntuaciones',
          component: puntuaciones,
          props: (query) => {
            return query.params},
        }
      ]
    }
  ]
})
